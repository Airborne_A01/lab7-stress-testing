# Lab7 -- Stress testing 

## Homework

As a homework you will need to open this [link](https://docs.google.com/spreadsheets/d/1lLigF6um8pKH2IGZNt9xXcl8quPtFPzmVtt5RCLeCqw/edit#gid=0), load test your service and provide the results of your test as a screenshot attached to the readme file with Artillery. Please establish how much rps you can get for (GetSpec, CalculatePrice with fixed_price plan, CalculatePrice with minute plan) and provide screenshots of the report and load testing execution(with user in bash shell) as well as your `test.yaml` file. 
**Lab is counted as done, if pipelines are passing. and tests are developped**

## Results

There will be 3 cases to test:

- get_spec
- fixed plan
- minute plan

### Load testing all at once for 10 minutes without high load

mean RPS = 38.95

![](imgs/total_1.png)
![](imgs/total_2.png)

## Testing specification retrieval

mean RPS = 90.37

![](imgs/minute_1.png)
![](imgs/minute_2.png)

### Testing fixed price plan

mean RPS = 109.84

> Forgot to output to file, but took screenshots of cmd output

![](imgs/fixed_1.png)
![](imgs/fixed_2.png)